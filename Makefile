# Straight forward Makefile to compile all examples in a row

# If cross-compiling, you may wish to set the following environment variable to the root location of 
# your 'sdk'
# SDKSTAGE=/home/foo/raspberrypi

INCDIR=-I./Common 
LIBDIR=-LOpenGL_SC_2_ICD

LIBS=-lGLESv2 -lEGL -lm -lX11 $(LIBDIR) -lGLSC2

CFLAGS+=-Ofast -ftree-vectorize -funroll-all-loops -g

COMMONSRC=./Common/esShader.c    \
          ./Common/esTransform.c \
          ./Common/esShapes.c    \
          ./Common/esUtil.c
COMMONSCSRC=./Common/scShader.c    \
          ./Common/esTransform.c \
          ./Common/esShapes.c    \
          ./Common/scUtil.c
COMMONHRD=esUtil.h
COMMONSCHRD=scUtil.h

VEC_ADD=vector_add.c
MAT_MULT=./mat_mult/mat_mult.c

default: all

all: vector_add/vector_add mat_mult/mat_mult

clean:
	find . -name "CH??_*" | xargs rm -f

./vector_add/vector_add: ${COMMONSRC} ${COMMONHDR} vector_add/${VEC_ADD}
	gcc $(CFLAGS) ${COMMONSRC} vector_add/${VEC_ADD} -o $@ ${INCDIR} ${LIBS}
./vector_add_sc2/vector_add: ${COMMONSCSRC} ${COMMONSCHDR} vector_add_sc2/${VEC_ADD}
	gcc $(CFLAGS) ${COMMONSCSRC} vector_add_sc2/${VEC_ADD} -o $@ ${INCDIR} ${LIBS}
./mat_mult/mat_mult: ${COMMONSRC} ${COMMONHDR} ${MAT_MULT}
	gcc $(CFLAGS) ${COMMONSRC} ${VEC_ADD} -o $@ ${INCDIR} ${LIBS}
